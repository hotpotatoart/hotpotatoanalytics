
let filtersDashboard = {
	city: {
		isActive: false,
		name: 'TOP 5'
	},
	players: {
		isActive: false,
		name: 'STAR PLAYERS'
	},
	gender: {
		isActive: false		
	},
	age: {
		isActive: false	
	},
	screens:{
		isActive: false,
		name: 'SCREENS'
	},
	firsDate: -1,
	endDate: -1
};

let dropDownList;
let bgDropDown;

let arrowYOY;
let graphicYOY;
let grephicYOYIsActive = false;

window.onload = ()=>{
	initComponents_dashboard();		
	prepareTimeLine();// hotpotato.js 
	updateMonthActive();// hotpotato.js
  filtersDashboard.firsDate = indexMonthStart;//variable de timeLine.js
  filtersDashboard.endDate = indexMonthEnd;//variable de timeLine.js  
};

function activedFilter (element) {	
	changeFilterStatus(element);	
}

function changeFilterStatus (element) {	
	if(activeFilterLine(element.id)){
		return;
	}
	switch (element.id) {
		case 'city':	
			changeStatusCity(element);	
			break;
		case 'players':	
			changeStatusPlayers(element);			
			break;
		case 'gender':			
			break;
		case 'age':						
			break;
		default:			
			break;
	}		
	graphicsManagement();	
}

function activeFilterLine (filterName) {
	filtersDashboard[filterName].isActive = !filtersDashboard[filterName].isActive;
	if(!filtersCheck()){
		filtersDashboard[filterName].isActive = !filtersDashboard[filterName].isActive;
		return true;
	}
	let textFilter = document.getElementById(`${filterName}`);			
	if(filtersDashboard[filterName].isActive) {		
		textFilter.style.textShadow = '0px 2px 10px white';
	} else {
		textFilter.style.textShadow = '0px 0px 0px white';
	}		
	return false;
}

function filtersCheck(){
	let filtersChecked = false;
	if (filtersDashboard.city.isActive && !filtersDashboard.players.isActive &&
    !filtersDashboard.gender.isActive && !filtersDashboard.age.isActive &&
    !filtersDashboard.screens.isActive) {
    	filtersChecked = true;
  } else if (!filtersDashboard.city.isActive && filtersDashboard.players.isActive &&
    !filtersDashboard.gender.isActive && !filtersDashboard.age.isActive &&
    !filtersDashboard.screens.isActive) {
    	filtersChecked = true;
  } else if (!filtersDashboard.city.isActive && !filtersDashboard.players.isActive &&
    filtersDashboard.gender.isActive && !filtersDashboard.age.isActive &&
    !filtersDashboard.screens.isActive) {
			filtersChecked = true;
  } else if (!filtersDashboard.city.isActive && !filtersDashboard.players.isActive &&
    !filtersDashboard.gender.isActive && filtersDashboard.age.isActive &&
    !filtersDashboard.screens.isActive) {
    	filtersChecked = true;
  } else if (!filtersDashboard.city.isActive && !filtersDashboard.players.isActive &&
    !filtersDashboard.gender.isActive && !filtersDashboard.age.isActive &&
    filtersDashboard.screens.isActive) {
    	filtersChecked = true;
  } else if (filtersDashboard.city.isActive && filtersDashboard.players.isActive &&
    !filtersDashboard.gender.isActive && !filtersDashboard.age.isActive &&
    !filtersDashboard.screens.isActive) {
			filtersChecked = true;
  } else if (filtersDashboard.city.isActive && !filtersDashboard.players.isActive &&
    !filtersDashboard.gender.isActive && !filtersDashboard.age.isActive &&
    filtersDashboard.screens.isActive) {
    	filtersChecked = true;
  } else if (!filtersDashboard.city.isActive && !filtersDashboard.players.isActive &&
    filtersDashboard.gender.isActive && filtersDashboard.age.isActive &&
    !filtersDashboard.screens.isActive) {
			filtersChecked = true;
  }  else if (!filtersDashboard.city.isActive && !filtersDashboard.players.isActive &&
    !filtersDashboard.gender.isActive && !filtersDashboard.age.isActive &&
    !filtersDashboard.screens.isActive) {
    	filtersChecked = true;
  }
	return filtersChecked;
}

function changeStatusCity (element) {
	addArrowDown('arrowDownCity',  element);
}

function changeStatusPlayers (element) {
	addArrowDown('arrowDownPlayers', element);	
}

function addArrowDown (arrow, element) {
	if(filtersDashboard[element.id].isActive) {
		document.getElementById(arrow).style.display = 'inline';			
		if(element.id === 'city' || element.id === 'players'){				
			element.setAttribute('ontouchmove', `showdropDownList(this, 'down')`);	
		} 		
	} else {
		document.getElementById(arrow).style.display = 'none';
		element.removeAttribute('ontouchmove');		
	}		
}

function showdropDownList (element, side) {		
	clearElement (dropDownList);			
	preparedropDownList(element.id);	
	let rectImg = element.getBoundingClientRect();
	let rectDropDawn = dropDownList.getBoundingClientRect();
	if(side == 'rightDown') {			
		dropDownList.style.left = `${rectImg.left}px`;	
		dropDownList.style.top = `${rectImg.bottom}px`;		
	} else if (side == 'down') {			
		dropDownList.style.left = `${rectImg.left}px`;	
		dropDownList.style.top = `${rectImg.bottom}px`;		
	}	
	bgDropDown.style.display = 'inline';
}

function clearElement (parentNode) {
	while(parentNode.hasChildNodes()) {
    parentNode.removeChild(parentNode.firstChild);        
  }
}

function preparedropDownList (idElement) {	
	loadItemsdropDownList (filtersTools[idElement].elements, idElement);	
}

function loadItemsdropDownList (itemList, filterName) {	
	let divLine;
	for (let i = 0; i < itemList.length; i++) {
		let span = document.createElement('span');
		span.className = 'itemDropDown';
		span.textContent = `${itemList[i].name}`;
		span.setAttribute("onclick", `selectItemOfdropDownList(${i},${filterName})`);
		dropDownList.appendChild(span);
		divLine = document.createElement('div');
		divLine.className = 'lineDropDown';
		dropDownList.appendChild(divLine);		
	}	
}

function selectItemOfdropDownList (index, element) {	
	const filterName = element.id;
	if(filterName == 'myGraphics') {
		// Neceitamos filterDashboard antes de modificarlo 
		//filtersDashboard = filtersTools[filterName].elements[index].config;
		updateFiltersActive(filterName,index);
	} else {
		filtersDashboard[filterName].name = `${filtersTools[filterName].elements[index].name}`;	
		graphicsManagement();
	}		
}

// Se ejecuta cuando un elemento de la lista de graficas se carga
function updateFiltersActive (graphicFilter,index) {
	for (let i = 0; i < Object.getOwnPropertyNames(filtersDashboard).length-2; i++) {
		let filterName =  Object.getOwnPropertyNames(filtersDashboard)[i];
		if(filtersDashboard[filterName].isActive != filtersTools[graphicFilter].elements[index].config[filterName].isActive ){
			let HtmlElement = document.getElementById(filterName);
			activedFilter(HtmlElement);
		}
	}
	let temp =JSON.parse( JSON.stringify(filtersTools[graphicFilter].elements[index].config ));
	filtersDashboard = temp;			
}

function hiddeDropDown(){
	bgDropDown.style.display = 'none';	
}

function initComponents_dashboard() {
	fillCitiesList();
	fillPlayersList();
	fillMyGraphicsList();
	fillScreensList();
	bgDropDown = document.getElementById('bgDropDown');		
	dropDownList = document.getElementById('dropDownList');		
	arrowYOY = document.getElementById('arrowYOY');
	graphicYOY = document.getElementById('graphicYOY');
}

function showYOY(){
	grephicYOYIsActive = !grephicYOYIsActive;
	if(grephicYOYIsActive){
		arrowYOY.style.transform = 'rotate(180deg)';
		graphicYOY.style.height = '50vh';
	} else {
		arrowYOY.style.transform = 'rotate(0deg)';
		graphicYOY.style.height = '0vh';
	}
	
}