

const d = new Date();
const date = {
  day: d.getDate(),
  month: d.getMonth(),
  year: d.getFullYear()
};
const matchInfo = {
  matchData: getMatchData(),
  matchCategories: getMatchCategories()
};

window.onload = ()=>{
  // las siguientes 2 lineas se deben eliminar
  initMatchGraphic();
  document.getElementById('returnMenu').style.display = 'inline';
};

document.addEventListener('deviceready', ()=>{
  changeOrientationApp('landscape', ()=>{
    setTimeout(() => {
      initMatchGraphic();
      document.getElementById('returnMenu').style.display = 'inline';
    }, 1000);
  });
}, false);

function returnMenu() {
  changeOrientationApp('portrait',()=>{
    document.getElementById('graphicMatches').style.display = 'none';
    document.getElementsByClassName('containerReturnMenu')[0].style.display = 'none';
    location.href = 'menu.html';
  });  
}

function initMatchGraphic() {
  createMatchGraphic(); 
}

function createMatchGraphic(){  
  Highcharts.chart('graphicMatches', {
    chart: {
      zoomType: 'x',
      backgroundColor:'transparent',   
    },    
    credits:false,
    title: {
      y: 30,
      text: 'Matchs',
      style: {
        color: 'rgba(255,255,255,1)',
        fontSize:'6vw',
        fontWeight: '400',
        fontFamily: 'Montserrat',
      }
    },  
    tooltip: {
			pointFormat: '<b>{point.y}</b> {series.name}',
			enabled:true   
    },
    xAxis: {
      lineColor: 'rgba(255,255,255,0.9)',
      lineWidth: 2,
      title: {
        offset: 0,
        x: -50,
        y: 30,
        enabled: true,
        text: 'Days',
        align: 'low',        
        style: {
          color: 'rgba(255,255,255,1)',
          fontFamily: 'Montserrat',
          fontSize:'4vw',
          fontWeight: '400'
        }        
      },
      categories: matchInfo.matchCategories,
      labels: {
        style: {
          color: 'rgba(255,255,255,0.7)',
          fontFamily: 'Montserrat',
          fontSize:'3vw',
          fontWeight: '400'
        }
      }
    },
    yAxis: {
      lineColor: 'rgba(255,255,255,0.9)',
      lineWidth: 2,
      title: {
        align: 'high',
        offset: 0,
        rotation: 0,
        y: -30,
        rotation:0,
        align: 'high',
        text: 'Sales',
        style:{
          fontFamily: 'Montserrat',
          color: 'rgba(255,255,255,1)',
          fontSize:'5vw',
          fontWeight: '400'
        }
      },
      labels: {
        style: {
          fontFamily: 'Montserrat',
          color: 'rgba(255,255,255,1)',
          fontSize:'3vw',
          fontWeight: '400'
        }
      }
    },
    colors: ['rgba(233,0,97,1)'],
    legend: {
      enabled: false
    },
    plotOptions: {
      area: {
        fillColor: {
          linearGradient: {
            x1: 0,
            y1: 0,
            x2: 0,
            y2: 1
          },
          stops: [
            [0, 'rgba(233,0,97,1)'],
            [1, Highcharts.Color('rgba(233,0,97,1)').setOpacity(0).get('rgba')],         
          ]
        },
        marker: {
          radius: 3,
          fillColor:'rgb(206,166,7)'
        },
        lineWidth: 3,
        states: {
          hover: {
            lineWidth: 1
          }
        },
        threshold: null
      }
    },
    series: [{
      type: 'area',
      name: 'Sales',
      data: matchInfo.matchData
    }]
  });

}// fin de la funcion create matchs graphic

function getMatchCategories(){
  const matchCategories = [
    ['1'],
    ['<b style="color:white; font-size: 3vw;font-Weight:700;">2</b>'],
    ['3'],
    ['4'],
    ['5'],
    ['6'],
    ['7'],
    ['8'],
    ['9'],
    ['10'],
    ['11'],
    ['12']
  ];
  return matchCategories;
}

function getMatchData(){
  const matchData = [
    ['Rusia vs Arabia Saudí', Math.abs(parseInt(Math.random()*1000000))],
    ['3 Matches', Math.abs(parseInt(Math.random()*1000000))],
    ['3 Matches', Math.abs(parseInt(Math.random()*1000000))],
    {
      name:'Mééico vs Alemania',
      y:Math.abs(parseInt(Math.random()*1000000)),      
      marker: {
          symbol: 'url(img/matches/banderaMexico.png)'
      }
    },
    ['3 Matches', Math.abs(parseInt(Math.random()*1000000))],
    ['3 Matches', Math.abs(parseInt(Math.random()*1000000))],
    ['3 Matches', Math.abs(parseInt(Math.random()*1000000))],
    ['3 Matches', Math.abs(parseInt(Math.random()*1000000))],
    ['3 Matches', Math.abs(parseInt(Math.random()*1000000))],
    {
      name:'Mééico vs Korea',
      y:Math.abs(parseInt(Math.random()*1000000)),
      marker: {
          symbol: 'url(img/matches/banderaMexico.png)'
      }
    },
    ['3 Matches', Math.abs(parseInt(Math.random()*1000000))],
    ['4 Matches', Math.abs(parseInt(Math.random()*1000000))],
    ['4 Matches', Math.abs(parseInt(Math.random()*1000000))],
    {
      name:'Mééico vs Suecia',
      y:Math.abs(parseInt(Math.random()*1000000)),
      marker: {
          symbol: 'url(img/matches/banderaMexico.png)'
      }
    },
    ['4 Matches', Math.abs(parseInt(Math.random()*1000000))]
  ];
  return matchData;
}
