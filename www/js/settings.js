

document.addEventListener("DOMContentLoaded", function(event) {
	document.formChangePassword.userName.value = userInfo.userName;
});

let currentPassword;
let newPassword;
let confirmNewPassword;

function ValidatePassword(){

	//Extraemos los campos de el formulario
	
	currentPassword=document.formChangePassword.currentPassword;
	newPassword=document.formChangePassword.newPassword;
	confirmNewPassword=document.formChangePassword.confirmNewPassword;

	if(currentPassword.value == ''|| newPassword.value == '' || confirmNewPassword.value == ''){
			showMessageDialog("Llene todos los campos.");
			return false;
	}

	//Extraer usuario y contraseña actual de local.storage y comparar con los datos
	if(currentPassword.value != userInfo.password){
		currentPassword.value ="";
		showMessageDialog("Contraseña Incorrecta");
		clearInput(currentPassword,newPassword,confirmNewPassword);
		return false;
	}
	//Patron para los numeros
	var patternNumeric=new RegExp("[0-9]+");
	//Patron para las letras
	var patterLetters=new RegExp("[a-zA-Z]+");
	//Patron para simbolos especiales, falita validar.
	var patterSimbol = new RegExp("[@#$]+");
	if(newPassword.value == confirmNewPassword.value){
		if(currentPassword.value != newPassword.value ){
			if(newPassword.value.length>=6){
				if(newPassword.value.search(patternNumeric)>=0 && newPassword.value.search(patterLetters)>=0){
						//retorno del servidor si todo salio bien
						return ChangePasswordIntoServer(newPassword.value);
				}else{
					showMessageDialog("La contraseña debe contener letras y numeros");
				}
			}else{
				showMessageDialog("La longitud mínima tiene que ser de 6 caracteres");
			}
		}else{
			showMessageDialog("La nueva contraseña no puede ser igual a la contraseña actal");
		}
	}else{
		  showMessageDialog("La nueva contraseña no coincide");
	}
	clearInput(currentPassword,newPassword,confirmNewPassword);
	return false;
}

function clearInput(currentPassword,newPassword,confirmNewPassword) {
	currentPassword.value ="";
	newPassword.value = "";
	confirmNewPassword.value = "";
}


function ChangePasswordIntoServer(newPsw){
	let sendInfo = btoa(JSON.stringify(userInfo));
	clearInput(currentPassword,newPassword,confirmNewPassword);
  let url = server + "/Analytics/v1/changePassword.php?data=" + sendInfo+"&newPassword="+btoa(newPsw);    
	let xhttp = new XMLHttpRequest(); 
  xhttp.open("GET", url, true);
  xhttp.send();  
  xhttp.onloadend = function(){
    if (this.readyState == 4 && this.status == 200) {
      let response = JSON.parse(this.responseText);            
      if(response.status) {
      	userInfo.password = newPsw;
      	sendInfo = btoa(JSON.stringify(userInfo));
        window.localStorage.setItem('userInfo', sendInfo);
        showMessageDialog("¡El cambio ha sido exitoso!");
        return response.status;
      } else {
				showMessageDialog(`Error: ${response.message}`);
      }
    } else {      
      // prompt: Error 
      showMessageDialog("Error de conexión");   
    }      
  };   
  return false; 
}




