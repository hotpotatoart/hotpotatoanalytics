

const infoContact = {
  name:'',
  phone:'',
  email:'',
  description:''
};

function contactUs() {
  const nameInput = document.getElementsByClassName('nameContactUs')[0];  
  const phoneInput = document.getElementsByClassName('phoneContactUs')[0];
  const emailInput = document.getElementsByClassName('emailContactUs')[0];
  const descriptionInput = document.getElementsByClassName('descriptionContactUs')[0]; 
  infoContact.name = nameInput.value;
  infoContact.phone = phoneInput.value;
  infoContact.email = emailInput.value;
  infoContact.description = descriptionInput.value;
  callServerFunctions(btoa(JSON.stringify(infoContact)), 'sendMail');
  setTimeout(()=>{
    nameInput.value = '';
    phoneInput.value = '';
    emailInput.value = '';
    descriptionInput.value = '';
  }, 500);    
}