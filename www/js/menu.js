const pages = {
	'Dashboard': 'dashboard.html',
	'My Boards': 'myboards.html',
	'Matches': 'matches.html',
	'Notifications': 'notification.html',
	'About': 'about.html',
	'Settings': 'settings.html'
};
let frame;
let menu;
let menuLeft;
let onMenuAnimation = false;
let onMenuAnimationLeft = false;
let touchShowMenu;
let touchMenu;
let touchMenuY;
let touchShowMenuLeft;
let touchMenuLeft;
let touchMenuLeftY;
let returnPortraitMode;

const positionMenu = {
	'showMenu':'0%',
	'hiddenMenu':'-30%'
};
const positionMenuLeft = {
	'showMenuLeft':'0%',
	'hiddenMenuLeft':'-50%'
};

window.onload = ()=>{	
	returnPortraitMode = document.getElementsByClassName('containerReturnPortrait')[0];
	deleteEventBackButton();	
	initComponents_menu();
	initComponents_MenuLeft();
	loadTouches();	
	loadPage(0);
};

document.addEventListener("deviceready", ()=>{	
	setTimeout(()=>{				
		//initDevice();
	},2000);
}, false);

function initDevice () {
	if(device.platform == 'iOS'){
		window.FirebasePlugin.grantPermission();
	}	
	window.FirebasePlugin.hasPermission(data=>{
		if(!data.isEnabled){
			alert(`No has dado permiso para recibir notificaciones`);
		} else {
			registerToken();		
		}	
	});
}

function initComponents_menu () {
	resetFiltersMenu();
	frame = document.getElementById('frame');	
	menu = document.getElementById('menu');	
}

function loadPage(page) {		
	const initialMenuFilter = document.getElementsByClassName('itemsMenu')[page];	
	imgItemsOnClick(initialMenuFilter);	
}

function imgItemsOnClick (element) {		
	if(onMenuAnimation) {
		return;
	}
	if(element.childNodes[1].textContent == 'Matches'){	
		resetFiltersMenu();
		const img = element.firstChild;
		changeStatusElement(img);		
		element.style.backgroundImage = 'url("img/menu/filterSelected.png")';	
		changeStatusMenu('hiddenMenu');
		setTimeout(() => {
			location.href = 'matches.html';	
		}, 900);			
		return;		
	}
	if(element.style.backgroundImage == ''){
		resetFiltersMenu();
		element.style.backgroundImage = 'url("img/menu/filterSelected.png")';	
		const img = element.firstChild;
		let source = getCurrentPath(`${img.src}`);
		let linkHtml = `${source.split('.')[0]}.html`;
		changeStatusElement(img);			
		frame.src = `${pages[element.childNodes[1].textContent]}`;
		setTimeout(()=>{
			changeStatusMenu('hiddenMenu');	
		}, 100);
	} else {
		changeStatusMenu('hiddenMenu');
	}			
}

function changeStatusMenu(changeStatus) {	
	onMenuAnimation = true;
	menu.style.animationName = changeStatus;
	setTimeout(()=>{
		menu.style.right = positionMenu[changeStatus];
		onMenuAnimation = false;
	}, 950);	
}

function changeStatusMenuLeft(changeStatus) {
	if(onMenuAnimationLeft) {
		return;
	}
	onMenuAnimationLeft = true;
	menuLeft.style.animationName = changeStatus;
	setTimeout(()=>{
		menuLeft.style.left = positionMenuLeft[changeStatus];
		onMenuAnimationLeft = false;
	}, 950);	
}


function logOut (element) {
	//firebaseUnregister();
	setTimeout(()=>{
		resetUserInfo();
		window.localStorage.setItem('userInfo', userInfo);
		location.href = 'login.html';	
	},500);	
}

function registerToken () {
	alert("start");
	window.FirebasePlugin.onTokenRefresh(function(token) {	  
	  alert(token);
	}, function(error) {
	  alert(error);
	});
	alert("end");
		/*
	window.FirebasePlugin.getToken( token => {
		alert(`${token}`);			
		let tokenDevice = '' + token;		
		if( tokenDevice != userInfo.token) {
			userInfo.token = tokenDevice;
			let sendInfo = btoa(JSON.stringify(userInfo));
			callServerFunctions(sendInfo, 'tokenRegister');
			window.localStorage.setItem('userInfo', sendInfo);
			alert('Se ha actualizado el token');
		}		
	}, error => {
	    alert(error);
	});
	*/
}

function firebaseUnregister () {
	window.FirebasePlugin.unregister();
	let sendInfo = btoa(JSON.stringify(userInfo));
	callServerFunctions(sendInfo, 'unregisterToken');
}

function resetFiltersMenu () {
	const buttonsFilter = document.getElementsByClassName('itemsMenu');
	for (let i = 0; i < buttonsFilter.length; i++) {
		buttonsFilter[i].style.backgroundImage = '';
		let srcImg = buttonsFilter[i].firstChild.src;
		srcImg = srcImg.split('/')[srcImg.split('/').length-1];
		if(srcImg.indexOf('2') > -1){
			buttonsFilter[i].firstChild.src = `img/menu/${srcImg.replace('2', '')}`;
		}
	}
}

function loadTouches() {
	const element = document.getElementById('touchRight');
	element.ontouchstart = touchShowMenuStart;
	element.ontouchend = touchShowMenuLast;

	menu.ontouchstart = touchMenuStart;
	menu.ontouchend = touchMenulast;

	const elementLeft = document.getElementById('touchLeft');
	elementLeft.ontouchstart = touchShowMenuLeftStart;
	elementLeft.ontouchend = touchShowMenuLeftLast;

	menuLeft.ontouchstart = menuLeftStart;
	menuLeft.ontouchend = menuLeftLast;
}

function menuLeftStart(evt) {
	touchMenuLeft = evt.touches[0].clientX;
	touchMenuLeftY = evt.touches[0].clientY;
}

function menuLeftLast(evt) {	
	const touchF = evt.changedTouches[0].clientX;
	const distance = Math.abs( evt.changedTouches[0].clientY - touchMenuLeftY);
	if(touchF < touchMenuLeft && distance < 20){
		changeStatusMenuLeft('hiddenMenuLeft');
	} 
}

function touchShowMenuLeftStart(evt) {
	touchShowMenuLeft = evt.touches[0].clientX;
}

function touchShowMenuLeftLast(evt) {	
	const touchF = evt.changedTouches[0].clientX;
	if(touchF > touchShowMenuLeft){
		changeStatusMenuLeft('showMenuLeft');
	} 
}

function touchMenuStart(evt) {
	touchMenu = evt.touches[0].clientX;
	touchMenuY = evt.touches[0].clientY;
}

function touchMenulast(evt) {	
	const touchF = evt.changedTouches[0].clientX;
	const distance = Math.abs( evt.changedTouches[0].clientY - touchMenuY);
	if(touchF > touchMenu && distance < 20){
		changeStatusMenu('hiddenMenu');
	} 
}

function touchShowMenuStart(evt) {
	touchShowMenu = evt.touches[0].clientX;
}

function touchShowMenuLast(evt) {	
	const touchF = evt.changedTouches[0].clientX;
	if(touchF < touchShowMenu){
		changeStatusMenu('showMenu');
	} 
}

function initComponents_MenuLeft() {
	createScreenGraphic('ScreensGraphic');	
	menuLeft = document.getElementById('menuLeft');	
}

function createScreenGraphic(element) {	
	Highcharts.chart(element, {
    chart: {
			backgroundColor:'transparent',
    	plotBackgroundColor: null,
      plotBorderWidth: null,
      plotShadow: false,
			type: 'pie',
			marginTop: 0
    },
    credits: false,
    title: {
			text: ''			
    },
    tooltip: {
			pointFormat: '<b>{point.percentage:.1f}%</b>',
			enabled:false   
    },
    plotOptions: {
      pie: {     
				borderWidth:1,  
				borderColor:'rgba(31,70,79,1)',
				size: '50%',   
        allowPointSelect: false,
        cursor: 'pointer',
        dataLabels: {
          enabled: true,
          format: '<span class="textGraphic">{point.percentage:.0f}%<span>',
          style: {
            color:'white',
						fontSize:'4vw',
						fontWeight:'100',
						textOutline:'0px'
          },                
					distance:0
        },
				showInLegend: false      
			}
		},
    colors: ['rgba(207, 222, 248,1)', 'rgba(253, 228, 201,1)', 'rgba(221, 248, 206,1)'],
    series: [{
      colorByPoint: true,
      data: [{
        name: 'AR',
        y: 100
      }, {
        name: 'Gallery',
        y: 40
      }, {
        name: 'Game',
        y: 90
      }]
    }]
	});	
}
