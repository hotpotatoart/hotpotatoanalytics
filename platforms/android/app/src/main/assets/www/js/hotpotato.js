


const server = "https://hotpotatoart.com";
let typeDialog;
let userInfo;
let localPath;
let body;

const filtersTools = {
	city: {
		elements: []
	},
	players: {
		elements: []
	},
	myGraphics: {
		elements: []
	},
	screens: {
		elements: []
	}
};

document.addEventListener("DOMContentLoaded", function (event) {
	initComponents();
});

function changeOrientationApp(orientationType, callFunction) {
	screen.orientation.unlock();
	screen.orientation.lock(orientationType).then(function (obj) {
		if (callFunction != undefined) {
			callFunction();
		}
	}, function (obj) {
		alert(obj);
		// el siguiente if se debe eliminar
		if (callFunction != undefined) {
			callFunction();
		}
	});
}

function callServerFunctions(sendInfo, filePHP) {
	let url = server + `/Analytics/v1/${filePHP}.php?data=` + sendInfo;
	let xhttp = new XMLHttpRequest();
	xhttp.open("GET", url, true);
	xhttp.send();
	xhttp.onloadend = function () {
		if (this.readyState == 4 && this.status == 200) {
			let objResponse = JSON.parse(this.responseText);
			if (objResponse.status) {
				alert(objResponse.message);
			} else {
				alert(objResponse.message);
			}
		} else {
			alert("Error de conexión");
		}
	};
}

function changeStatusElement(element) {
	let pathImage = getCurrentPath(`${location}`);
	pathImage = pathImage.split('.')[0];
	let imgItemPath = `img/${pathImage}/`;
	let source = getCurrentPath(`${element.src}`);
	let indexChar = source.indexOf('2');
	if (indexChar > -1) {
		source = source.replace('2', '');
	} else {
		source = `${source.split('.')[0]}2.png`;
	}
	source = imgItemPath + source;
	element.src = source;
}

function resetStatusElement(element) {
	let pathImage = getCurrentPath(`${location}`);
	pathImage = pathImage.split('.')[0];
	let imgItemPath = `img/${pathImage}/`;
	let source = getCurrentPath(`${element.src}`);
	let indexChar = source.indexOf('2');
	if (indexChar > -1) {
		source = source.replace('2', '');
	}
	source = imgItemPath + source;
	element.src = source;
}

function checkAcountWithServer() {
	let sendInfo = btoa(JSON.stringify(userInfo));
	let url = server + "/Analytics/v1/loginAnalytics.php?data=" + sendInfo;
	let xhttp = new XMLHttpRequest();
	xhttp.open("GET", url, true);
	xhttp.send();
	xhttp.onloadend = function () {
		if (this.readyState == 4 && this.status == 200) {
			let response = JSON.parse(this.responseText);
			if (response.status) {
				window.localStorage.setItem('userInfo', sendInfo);
				location.href = 'menu.html';
			} else {
				if (localPath == 'login.html') {
					navigator.vibrate([20]);
					navigator.vibrate([20]);
					resetUserInfo();
					showMessageDialog("Datos incorrectos");
				} else {
					navigator.vibrate([20]);
					location.href = 'login.html';
				}
			}
		} else {
			// prompt: Error 
			alert("Error de conexión");
			location.href = 'login.html';
		}
	};
}

function cleanUser() {
	resetUserInfo();
	let sendInfo = btoa(JSON.stringify(userInfo));
	window.localStorage.setItem('userInfo', sendInfo);
}

function initComponents() {
	body = document.getElementsByTagName('body')[0];
	resetUserInfo();
	getAcount();
	localPath = getCurrentPath(`${location}`);
	createTypeDialog();
}

function resetUserInfo() {
	userInfo = {
		userName: '',
		password: '',
		token: ''
	};
}

function getAcount() {
	let userData64 = window.localStorage.getItem('userInfo');
	if (userData64 != null) {
		try {
			userInfo = JSON.parse(atob(userData64));
		} catch (error) {
			cleanUser();
		}
	}
}

function createTypeDialog() {
	typeDialog = [{
		"url": "img/generales/aviso.png",
		"titulo": "Notificación",
		"botones": ["Aceptar"],
	},
	{
		"url": "img/generales/precaucion.png",
		"titulo": "Confirmación",
		"botones": ["Aceptar", "Cancelar"]
	}
	];
}

function getCurrentPath(path) {
	let elementsPath = path.split('/');
	return elementsPath[elementsPath.length - 1];
}

function deleteEventBackButton() {
	document.addEventListener("backbutton", onBackKeyDown, false);
}

function onBackKeyDown() { }

function sendPasswordToEmail() {
	let mail = formSendEmail.inputEmail.value;
	var popup = document.getElementById("myPopup");
	if (mail == '') {
		popup.textContent = "Complete el campo";
		popup.classList.add("show");
		return false;
	} else {
		if (!validarEmail(mail)) {
			popup.textContent = "Correo Invalido";
			popup.classList.add("show");
			return false;
		}
		popup.classList.remove("show");
	}
	closeDialogWindow();
	sendData(mail);
}

function sendData(mail) {
	let sendInfo = btoa(`{"email":"${mail}"}`);
	let url = server + "/Analytics/v1/forgotPassword.php?data=" + sendInfo;
	let xhttp = new XMLHttpRequest();
	xhttp.open("GET", url, true);
	xhttp.send();
	xhttp.onloadend = function () {
		if (this.readyState == 4 && this.status == 200) {
			let response = JSON.parse(this.responseText);
			if (response.status) {
				showMessageDialog("¡Por favor revise su bandeja de entrada!\n" + mail);
			} else {
				showMessageDialog("Correo no registrado");
			}
		} else {
			showMessageDialog("error de internet");
		}
	};
}

//#region ShowDialog

function showCautionMessageDialog(message, action) {
	genericMessageDialog(message, action, 1);
}

function showMessageDialog(message, action) {
	genericMessageDialog(message, action, 0);
}

function showInputMessageDialog() {
	dialog = dialogTemplete();

	let contentBody = document.createElement('div');
	contentBody.className = 'contentBodyInput';

	let contentImg = document.createElement('div');
	contentImg.className = 'contentImg';
	const imgMail = document.createElement('img');
	imgMail.className = 'imgMail';
	imgMail.src = 'img/generales/mail.png';
	contentImg.appendChild(imgMail);

	let contentLabel = document.createElement('div');
	contentLabel.className = 'contentLabel';
	let labelBody = document.createElement('label');
	labelBody.textContent = "Ingresa tu correo eléctronico";
	contentLabel.appendChild(labelBody);

	let formEmail = document.createElement('form');
	formEmail.className = "formEmail";
	formEmail.name = "formSendEmail";

	let popUp = document.createElement('span');
	popUp.className = "popuptext";
	popUp.id = "myPopup";
	popUp.textContent = "Campo necesario";
	popUp.onclick = ocultarPopUp;

	let divPopup = document.createElement('div');
	divPopup.className = 'popup';
	divPopup.appendChild(popUp);
	formEmail.appendChild(divPopup);

	let inputMail = document.createElement('input');
	inputMail.className = 'inputMail';
	inputMail.name = 'inputEmail';
	inputMail.type = "email";
	inputMail.required = "required";
	inputMail.placeholder = "E-Mail";
	inputMail.autocomplete = "off";
	inputMail.onfocus = ocultarPopUp;
	formEmail.appendChild(inputMail);


	let buttonSend = document.createElement('div');
	buttonSend.className = 'buttonSend';
	buttonSend.textContent = 'Enviar';
	buttonSend.onclick = sendPasswordToEmail;
	formEmail.appendChild(buttonSend);


	contentBody.appendChild(contentImg);
	contentBody.appendChild(contentLabel);
	contentBody.appendChild(formEmail);
	dialog.appendChild(contentBody);
}

function dialogTemplete() { // Se llamaba prompt 
	body.style.overflowY = 'hidden';
	//Creacion generica
	const dialogWindow = document.createElement('div');
	dialogWindow.id = 'windowPrompt';
	dialogWindow.className = 'containerDialog';
	const dialog = document.createElement('div');
	dialog.className = 'dialog';
	// integracion de Imagen
	const imgClose = document.createElement('img');
	imgClose.onclick = closeDialogWindow;
	imgClose.className = 'imgclose';
	imgClose.src = 'img/generales/cerrar.png';
	// integracion a body
	dialog.appendChild(imgClose);
	dialogWindow.appendChild(dialog);
	body.appendChild(dialogWindow);

	return dialog;
}

function genericMessageDialog(message, action, option) {
	dialog = dialogTemplete();

	let contentBody = document.createElement('div');
	contentBody.className = 'contentBody';
	let contentTitulo = document.createElement('div');
	contentTitulo.className = 'contentTitulo';

	let labelTitulo = document.createElement('label');
	labelTitulo.textContent = typeDialog[option].titulo;
	contentTitulo.appendChild(labelTitulo);
	let contentImg = document.createElement('div');
	contentImg.className = 'contentImg';
	contentImg.style.marginLeft = "43%";
	const imgIcon = document.createElement('img');
	imgIcon.className = 'imgIcon';
	imgIcon.src = typeDialog[option].url;
	contentImg.appendChild(imgIcon);
	let contentLabel = document.createElement('div');
	contentLabel.className = 'contentLabel';
	let labelBody = document.createElement('label');
	labelBody.textContent = message;
	contentLabel.appendChild(labelBody);
	let formButtons = createButtons(action, typeDialog[option].botones.length, option);
	contentBody.appendChild(contentTitulo);
	contentBody.appendChild(contentImg);
	contentBody.appendChild(contentLabel);
	contentBody.appendChild(formButtons);
	dialog.appendChild(contentBody);

}

function createButtons(action = "closeDialogWindow()", numBottons, option) {
	let formButtons = document.createElement('form');
	formButtons.className = "formButtons";
	let buttons = new Array(numBottons);
	for (let index = 0; index < numBottons; index++) {
		buttons[index] = document.createElement('div');
		buttons[index].className = 'button';
		let labelTxt = document.createElement('label');
		labelTxt.className = "buttonText";
		labelTxt.textContent = typeDialog[option].botones[index];
		if (index == 0)
			buttons[index].setAttribute('onclick', action);
		else
			buttons[index].onclick = closeDialogWindow;
		buttons[index].appendChild(labelTxt);
		formButtons.appendChild(buttons[index]);
	}

	if (numBottons == 1)
		buttons[0].style.marginLeft = "37%";

	return formButtons;
}

function closeDialogWindow() {
	const dialog = document.getElementById('windowPrompt');
	body.removeChild(dialog);
	body.style.overflowY = '';
}

//#region PopUp
function ocultarPopUp() {
	var popup = document.getElementById("myPopup");
	popup.classList.remove("show");

}
//#endregion

//#region Validad Correo

function validarEmail(email) {
	var regex = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
	return regex.test(email) ? true : false;
}

//#endregion

//#endregion

function fillMyGraphicsList() {
	// debería de llamar al servidor para llenar estas listas
	filtersTools.myGraphics.elements = [{
		name: 'My board 1',
		config: {
			city: {
				isActive: true,
				name: 'TOP 5'
			},
			product: {
				isActive: false,
				name: 'STAR PRODUCTS'
			},
			gender: {
				isActive: false
			},
			age: {
				isActive: false
			},
			firsDate: 0,
			endDate: 5
		}
	},
	{
		name: 'My board 2',
		config: {
			city: {
				isActive: false,
				name: 'CDMX'
			},
			product: {
				isActive: true,
				name: 'RUFFLES'
			},
			gender: {
				isActive: true
			},
			age: {
				isActive: true
			},
			firsDate: 0,
			endDate: 7
		}
	},
	{
		name: 'My board 3, México, Ochoa',
		config: {
			city: {
				isActive: true,
				name: 'CDMX'
			},
			product: {
				isActive: true,
				name: 'RUFFLES'
			},
			gender: {
				isActive: false
			},
			age: {
				isActive: true
			},
			firsDate: 0,
			endDate: 7
		}
	},
	{
		name: 'My board 4, Monterrey, Chicharito',
		config: {
			city: {
				isActive: false,
				name: 'CDMX'
			},
			product: {
				isActive: true,
				name: 'RUFFLES'
			},
			gender: {
				isActive: true
			},
			age: {
				isActive: false
			},
			firsDate: 0,
			endDate: 7
		}
	},
	{
		name: 'My board 5',
		config: {
			city: {
				isActive: false,
				name: 'CDMX'
			},
			product: {
				isActive: false,
				name: 'RUFFLES'
			},
			gender: {
				isActive: true
			},
			age: {
				isActive: true
			},
			firsDate: 0,
			endDate: 7
		}
	},
	{
		name: 'My board 4, Monterrey, Chicharito',
		config: {
			city: {
				isActive: false,
				name: 'CDMX'
			},
			product: {
				isActive: true,
				name: 'RUFFLES'
			},
			gender: {
				isActive: true
			},
			age: {
				isActive: false
			},
			firsDate: 0,
			endDate: 7
		}
	},
	{
		name: 'My board 4, Monterrey, Chicharito',
		config: {
			city: {
				isActive: false,
				name: 'CDMX'
			},
			product: {
				isActive: true,
				name: 'RUFFLES'
			},
			gender: {
				isActive: true
			},
			age: {
				isActive: false
			},
			firsDate: 0,
			endDate: 7
		}
	},
	{
		name: 'My board 4, Monterrey, Chicharito',
		config: {
			city: {
				isActive: false,
				name: 'CDMX'
			},
			product: {
				isActive: true,
				name: 'RUFFLES'
			},
			gender: {
				isActive: true
			},
			age: {
				isActive: false
			},
			firsDate: 0,
			endDate: 7
		}
	}
	];
}

function fillScreensList() {
	filtersTools.screens.elements = [{
		name: "screens",
		screensName: [
			"AUGMENTED REALITY",
			"MINI GAME",
			"GALLERY"
		]
	}];
}

function fillPlayersList() {
	filtersTools.players.elements = [{
		name: "STAR PLAYERS",
		players: [
			"GUARDADO",
			"JONATHAN",
			"CHICHARITO"
		]
	},
	{
		name: "ALANIS"
	},
	{
		name: "GOVEA"
	},
	{
		name: "FABIÁN"
	},
	{
		name: "ARAUJO"
	},
	{
		name: "REYES"
	},
	{
		name: "VELA"
	},
	{
		name: "TECATITO"
	},
	{
		name: "SALCEDO"
	},
	{
		name: "PERALTA"
	},
	{
		name: "MORENO"
	},
	{
		name: "LOZANO"
	},
	{
		name: "HERRERA"
	},
	{
		name: "DAMM"
	},
	{
		name: "JONATHAN"
	},
	{
		name: "OCHOA"
	},
	{
		name: "CORONA"
	},
	{
		name: "LAYÚN"
	},
	{
		name: "GIOVANI"
	},
	{
		name: "GALLARDO"
	},
	{
		name: "AYALA"
	},
	{
		name: "ANTUNA"
	},
	{
		name: "CHICHARO"
	},
	{
		name: "JIMÉNEZ"
	},
	{
		name: "GUARDADO"
	},
	{
		name: "AQUINO"
	}
	];
}

function fillCitiesList() {
	filtersTools.city.elements = [{
		name: "TOP 5",
		cities: [
			"CDMX",
			"JALISCO",
			"NUEVO LEÓN",
			"CHIHUAHUA",
			"MÉXICO"
		]
	},
	{
		name: "AGUAS CALIENTES"
	},
	{
		name: "BAJA CALIFORNIA"
	},
	{
		name: "BAJA CALIFORNIA SUR"
	},
	{
		name: "CAMPECHE"
	},
	{
		name: "CHIAPAS"
	},
	{
		name: "CHIHUAHUA"
	},
	{
		name: "CDMX"
	},
	{
		name: "COAHUILA"
	},
	{
		name: "COLIMA"
	},
	{
		name: "DURANGO"
	},
	{
		name: "MÉXICO"
	},
	{
		name: "GUANAJUATO"
	},
	{
		name: "GUERRERO"
	},
	{
		name: "HIDALGO"
	},
	{
		name: "JALISCO"
	},
	{
		name: "MICHOACÁN"
	},
	{
		name: "MORELOS"
	},
	{
		name: "NAYARIT"
	},
	{
		name: "NUEVO LEÓN"
	},
	{
		name: "OAXACA"
	},
	{
		name: "PUEBLA"
	},
	{
		name: "QUERÉTARO"
	},
	{
		name: "QUINTANA ROO"
	},
	{
		name: "SAN LUIS POTOSÍ"
	},
	{
		name: "SINALOA"
	},
	{
		name: "SONORA"
	},
	{
		name: "TABASCO"
	},
	{
		name: "TAMAULIPAS"
	},
	{
		name: "TLAXCALA"
	},
	{
		name: "VERACRUZ"
	},
	{
		name: "YUCATÁN"
	},
	{
		name: "ZACATECAS"
	}
	];
}

