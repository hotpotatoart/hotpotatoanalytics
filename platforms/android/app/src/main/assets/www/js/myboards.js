

window.onload = ()=>{  
  initMyBoards();
};

function initMyBoards() {
	fillMyGraphicsList();
  const existBoards = getMyBoards();
  if(!existBoards){
    alert('No hay configuraciones guardadas');
    retirn;
  }
 
  const containerBoards = document.createElement('div');  
  let board;
  let boardName;
  let imgEdit;
  let imgDelete;
  
  for(let i = 0; i < filtersTools.myGraphics.elements.length; i++){
    board = document.createElement('div'); 
    boardName = document.createElement('div');
    imgEdit = document.createElement('img');  
    imgDelete = document.createElement('img');

    containerBoards.className = 'containerBoards';
    board.className = 'boards';  
    boardName.className = 'boardName';
    boardName.textContent = filtersTools.myGraphics.elements[i].name;
    imgEdit.className = 'imgEdit';  
    imgEdit.src = 'img/myboards/edit.png';  
    imgDelete.className = 'imgDelete';
    imgDelete.src = 'img/myboards/delete.png';

    imgDelete.onclick = ()=>{
      alert(`Borrar Grafica ${i}`);
    };

    imgEdit.onclick = ()=>{
      alert(`Editar la gráfica ${i}`);
    };

    if(i % 2 == 0 || i == 0){
      board.style.backgroundColor = 'rgba(104,153,152,0.3)';      
    } else {
      board.style.backgroundColor = 'rgba(178,189,195,0.3)';
    }

    board.appendChild(boardName);  
    board.appendChild(imgEdit);
    board.appendChild(imgDelete);

    containerBoards.appendChild(board);
  }

  body.appendChild(containerBoards);
  
}

function getMyBoards() {
  // se debe hacer peticion al servidor y regresar la lista de configuraciones  
  return true;
}