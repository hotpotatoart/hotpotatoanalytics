//#region Variables Globales
let dataFireBase;
//#endregion

//#region Variables Plantilla
let notificationsFromServer;
let divNotificationContent;
let notifications;
let plantilla;
let divFather;
//#endregion

//#region Variables Notifiacion Emergente
let htmlAddEmergente;
let htmlFooterLogo;
let checkOptions;
let selectList;
let listInputRange;
let indexEditNotification;
let arryChecks;
//#endregion

//#region Variables HTML
let inpName;
let cLocation;
let cScreens;
let cPlayers;
let panelLoading;
//#endregion

//#region Windows Load
window.onload = () => {
    panelLoading = document.getElementById("panelLoading");
    initComponentsNotification();
    divNotificationContent = document.getElementById("divNoticationsContent");
    plantilla = document.getElementById("plantilla");
    htmlAddEmergente = document.getElementById('addNotificationEmergente');
    htmlFooterLogo = document.getElementById('footerLogo');
    inpName = document.addNotificationEmergente.inputName;
    cLocation = document.addNotificationEmergente.checkLocation;
    cScreens = document.addNotificationEmergente.checkScreens;
    cPlayers = document.addNotificationEmergente.checkPlayers;
    arryChecks = [cLocation, cScreens, cPlayers];
    indexEditNotification = -1;
};
//#endregion


//#region Notification
function loadNotifications() {
    divFather = document.createElement('div');
    if (notificationsFromServer != undefined && notificationsFromServer.length != 0) {
        let countNotifications = notificationsFromServer.length;
        notifications = new Array(countNotifications);
        plantilla.style.display = "block";
        for (let i = 0; i < countNotifications; i++) {
            document.getElementById("tituloNotification").textContent = notificationsFromServer[i].title;
            if (notificationsFromServer[i].filters.ubication[0]) {
                document.getElementById("filterContent1").style.display = "block";
                document.getElementById("filterUbication").textContent = notificationsFromServer[i].filters.ubication[1];
            } else {
                document.getElementById("filterContent1").style.display = "none";
            }
            if (notificationsFromServer[i].filters.screen[0]) {
                document.getElementById("filterContent2").style.display = "block";
                document.getElementById("filterScreen").textContent = notificationsFromServer[i].filters.screen[1];
            } else {
                document.getElementById("filterContent2").style.display = "none";
            }
            if (notificationsFromServer[i].filters.players[0]) {
                document.getElementById("filterContent3").style.display = "block";
                document.getElementById("filterPlayers").textContent = notificationsFromServer[i].filters.players[1];
            } else {
                document.getElementById("filterContent3").style.display = "none";
            }
            document.getElementById("limInf").textContent = intToKs(notificationsFromServer[i].limits[0]);
            document.getElementById("imgFlechaInv").style.transform = "rotate(-180deg)";
            document.getElementById("limSup").textContent = intToKs(notificationsFromServer[i].limits[1]);
            document.getElementById("edit").setAttribute('onclick', `editNotification(${i})`);
            document.getElementById("delete").setAttribute('onclick', `confirmActionNotification(${i})`);
            notifications[i] = plantilla.cloneNode(true);
            notifications[i].id = `${i}`;
            divFather.appendChild(notifications[i]);
        }
    } else {
        let divEmpty = document.createElement('div');
        divEmpty.className = "noNotif";
        let labelEmpty = document.createElement('label');
        labelEmpty.textContent = "Create New Notification";
        divEmpty.appendChild(labelEmpty);
        divFather.appendChild(divEmpty);

        document.getElementById("logo").style.visibility = "hidden";
        let logoFloat = document.createElement('div');
        logoFloat.className = "floatLogo";
        logoFloat.id = "floatLogo";
        let imgLogoFloat = document.createElement('img');
        imgLogoFloat.className = "imgFloatLogo";
        imgLogoFloat.src = "img/notification/logo.png";

        logoFloat.appendChild(imgLogoFloat);

        document.getElementById("body_notification").appendChild(logoFloat);
    }
    let divAdd = document.createElement('div');
    divAdd.className = "imgContentMore";
    let imgAdd = document.createElement('img');
    imgAdd.className = "imgMore";
    imgAdd.src = "img/notification/agregar.png";
    imgAdd.onclick = showAddNotificationWindow;
    divAdd.appendChild(imgAdd);
    divFather.appendChild(divAdd);
    divNotificationContent.appendChild(divFather);
    plantilla.style.display = "none";
    togglePanelLoading();
}

function confirmActionNotification(indexNotification) {
    var anonimFunction = `removeNotification(${indexNotification})`;
    showCautionMessageDialog("Confirme que desea borrar la notificacion.", anonimFunction);
}

function intToKs(number) {
    var nameK;
    if ((number / 1000000) >= 1)
        nameK = `${number/1000000}M`;
    else if ((number / 1000) >= 1)
        nameK = `${number/1000}K`;
    else
        nameK = number.toString();

    return nameK;
}

function editNotification(indexNotification) {
    let arryChecks = [cLocation, cScreens, cPlayers];
    let singleNotification = notificationsFromServer[indexNotification];
    inpName.value = singleNotification.title;


    for (let index = 0; index < Object.getOwnPropertyNames(singleNotification.filters).length; index++) {
        const element = Object.getOwnPropertyNames(singleNotification.filters)[index];
        if (singleNotification.filters[element][0]) {
            validationCheckBox(arryChecks[index]);
            arryChecks[index].checked = true;
            let htmlSelect = document.getElementById("selectText" + checkOptions[arryChecks[index].name].name);
            htmlSelect.textContent = singleNotification.filters[element][1];
        }

    }

    let htmlRangeMin = document.addNotificationEmergente.rangeMin;
    let htmlRangeMax = document.addNotificationEmergente.rangeMax;
    document.addNotificationEmergente.inputMin.value = `${singleNotification.limits[0]/1000}K`;
    document.addNotificationEmergente.inputMax.value = `${singleNotification.limits[1]/1000000}M`;
    htmlRangeMin.value = singleNotification.limits[0] / 1000;
    htmlRangeMax.value = singleNotification.limits[1] / 1000000;

    indexEditNotification = indexNotification;
    showAddNotificationWindow();
}

function removeNotification(index) {
    closeDialogWindow();
    divNotificationContent.removeChild(divFather);
    removeNotificationServer(notificationsFromServer[index]);
    notificationsFromServer.splice(index, 1);
    loadNotifications();

}

//#endregion

//#region AddNotification

function showAddNotificationWindow() {
    htmlFooterLogo.style.visibility = "hidden";
    htmlAddEmergente.style.display = "block";
}

function closeAddNotification() {
    htmlAddEmergente.style.display = "none";
    htmlFooterLogo.style.visibility = "visible";
    resetAddNotification();
}

function addNotification() {
    if (inpName.value != '') {
        if (cLocation.checked || cScreens.checked || cPlayers.checked) {
            for (let i = 0; i < Object.getOwnPropertyNames(checkOptions).length; i++) {
                let checkName = Object.getOwnPropertyNames(checkOptions)[i];
                if (checkOptions[checkName].disable) {
                    let htmlSelectText = document.getElementById("selectText" + checkOptions[checkName].name);
                    if (htmlSelectText.textContent == checkOptions[checkName].title) {
                        showMessageDialog(`Seleccione un elemento ${checkOptions[checkName].name}`);
                        return;
                    }
                }

            }
            addNotificationList();
            closeAddNotification();
            resetAddNotification();
            if (indexEditNotification == -1) {
                //showMessageDialog("Add Notifiction Succesfull");
            } else {
                //showMessageDialog("Edit Notification Succesfull");
                indexEditNotification = -1;
            }
        } else {
            showMessageDialog("Debe seleccionar por lo menos un filtro");
        }
    } else {
        showMessageDialog("El Nombre es necesario");
    }

    return 0;

}

function addNotificationList() {
    if (notificationsFromServer == undefined)
        notificationsFromServer = new Array();

    let structNotification = getDataForm();

    if (indexEditNotification == -1) {
        notificationsFromServer.push(structNotification);
        addNotificationServer(structNotification);

    } else {
        editNotificationServer(notificationsFromServer[indexEditNotification], structNotification);
        notificationsFromServer[indexEditNotification] = structNotification;
    }
    
    if (notificationsFromServer.length == 1 && indexEditNotification == -1) {
        
        let logoFloat = document.getElementById('floatLogo');
        document.getElementById('body_notification').removeChild(logoFloat);
        document.getElementById("logo").style.visibility = "visible";
        
    }
    divNotificationContent.removeChild(divFather);
    loadNotifications();
}

function getDataForm() {
    let arraySelects = new Array();
    let htmlRangeMin = document.addNotificationEmergente.rangeMin;
    let htmlRangeMax = document.addNotificationEmergente.rangeMax;
    for (let i = 0; i < Object.getOwnPropertyNames(checkOptions).length; i++) {
        let checkName = Object.getOwnPropertyNames(checkOptions)[i];
        if (checkOptions[checkName].disable) {
            let htmlSelect = document.getElementById("selectText" + checkOptions[checkName].name);;
            arraySelects.push(htmlSelect.textContent);
        } else {
            arraySelects.push("");
        }
    }
    let dataForm = {
        "title": inpName.value,
        "filters": {
            "ubication": [cLocation.checked, arraySelects[0]],
            "screen": [cScreens.checked, arraySelects[1]],
            "players": [cPlayers.checked, arraySelects[2]]
        },
        "limits": [htmlRangeMin.value * 1000, htmlRangeMax.value * 1000000]
    };
    return dataForm;
}

function resetAddNotification() {
    inpName.value = "";
    let htmlRangeMin = document.addNotificationEmergente.rangeMin;
    let htmlRangeMax = document.addNotificationEmergente.rangeMax;
    document.addNotificationEmergente.inputMin.value = "50K";
    document.addNotificationEmergente.inputMax.value = "20M";
    htmlRangeMin.value = 50;
    htmlRangeMax.value = 20;

    if (cLocation.checked)
        validationCheckBox(cLocation);
    if (cScreens.checked)
        validationCheckBox(cScreens);
    if (cPlayers.checked)
        validationCheckBox(cPlayers);
    cLocation.checked = false;
    cScreens.checked = false;
    cPlayers.checked = false;
}

function validationCheckBox(evt) {
    checkOptions[`${evt.name}`].disable = !checkOptions[`${evt.name}`].disable;
    if (evt.name != "checkLocation") {
        document.addNotificationEmergente[checkOptions[`${evt.name}`].check].disabled = checkOptions[`${evt.name}`].disable;
    }

    if (checkOptions[`${evt.name}`].disable)
        loadOptions(evt, dataFireBase,"selectedOption");
    else
        removeOption(evt);
    hiddenOtherSelect('');
}

//#endregion


//#region Input Range
function changeRangeValue(inputRange) {
    htmlInputText = document.addNotificationEmergente[listInputRange[`${inputRange.name}`]];
    htmlInputText.value = inputRange.value + listInputRange[`${inputRange.id}`];
}
//#endregion

//#region InitComponents

function initComponentsNotification() {
    togglePanelLoading()
    initDataFireBase();
    initNotifications();
    initCheckOptions();
    initListInput();
}

function initDataFireBase() {
    dataFireBase = {
        "checkLocation": [
            "Mexico",
            "CDMX",
            "Puebla",
            "Sinaloa",
            "Guadalajara",
            "Mas.."
        ],
        "checkScreens": [
            "Principal AR",
            "Gallery",
            "Game",
            "FAQs"
        ],
        "checkPlayers": [
            "Ochoa",
            "Moreno",
            "Chicharito",
            "Guardado",
            "Oribe"
        ]

    }
}

function initCheckOptions() {
    checkOptions = {
        "checkLocation": {
            "disable": false,
            "check": "",
            "selectID": "filter1",
            "visible": "visible",
            "title": "Location...",
            "name": "Location",
            "activeFilter": false
        },
        "checkScreens": {
            "disable": false,
            "check": "checkPlayers",
            "selectID": "filter2",
            "visible": "visible",
            "title": "Screen...",
            "name": "Screens",
            "activeFilter": false
        },
        "checkPlayers": {
            "disable": false,
            "check": "checkScreens",
            "selectID": "filter2",
            "visible": "visible",
            "title": "Player...",
            "name": "Players",
            "activeFilter": false
        }
    };

    selectList = {
        filter1: new Object(),
        filter2: new Object()
    };
}

function initListInput() {
    listInputRange = {
        "rangeMin": "inputMin",
        "rangeMax": "inputMax",
        "min": "K",
        "max": "M"
    };
}


//#endregion

//#region HTTP Request

function initNotifications() {

    let sendInfo = btoa(JSON.stringify(userInfo));
    let url = server + "/Analytics/v1/getNotifications.php?data=" + sendInfo;
    let xhttp = new XMLHttpRequest();
    xhttp.open("GET", url, true);
    xhttp.send();
    xhttp.onloadend = function () {
        if (this.readyState == 4 && this.status == 200) {
            let response = JSON.parse(this.responseText);
            if (response[0].status) {
                response.splice(0, 1);
                notificationsFromServer = JSON.parse(JSON.stringify(response));
                loadNotifications();
            } else {
                alert("Error al consultar DB");
            }
        } else {
            // prompt: Error 
            alert("Error de conexión");
        }
    };
}

function addNotificationServer(structSend) {
    let dataJson = JSON.parse(JSON.stringify(structSend));
    httpRequest(dataJson, 'addNotification.php');
}

function editNotificationServer(strucCuurent, newStruct) {
    if (JSON.stringify(strucCuurent) != JSON.stringify(newStruct)) {
        let arrayInfo = new Array();
        arrayInfo.push(JSON.parse(JSON.stringify(strucCuurent)));
        arrayInfo.push(JSON.parse(JSON.stringify(newStruct)));
        httpRequest(arrayInfo, 'editNotification.php');
    }else{
        togglePanelLoading();
    }
}

function removeNotificationServer(structSend) {
    let dataJson = JSON.parse(JSON.stringify(structSend));
    httpRequest(dataJson, 'removeNotification.php');
}



function httpRequest(dataJson, filePHP) {
    let copySendInfo = JSON.parse(JSON.stringify(userInfo));
    let arrayInfo = new Array();
    arrayInfo.push(copySendInfo);
    arrayInfo.push(dataJson);
    let url = server + "/Analytics/v1/" + filePHP + "?data=" + btoa(JSON.stringify(arrayInfo));
    console.log(url);
    let xhttp = new XMLHttpRequest();
    xhttp.open("GET", url, true);
    xhttp.send();
    xhttp.onloadend = function () {
        if (this.readyState == 4 && this.status == 200) {
            let response = JSON.parse(this.responseText);
            togglePanelLoading();
            if (response.status) {
                showMessageDialog(response.message);
            } else {
                showMessageDialog(response.message);
            }
        } else {
            // prompt: Error 
            alert("Error de conexión");
        }
    };
}

//#endregion


//#region Hotpotato
function togglePanelLoading(){
    panelLoading.classList.toggle('showPanel');
}
//#endregion

//#region Select
function loadOptions(evt, dataToLoad, functionName) {
    let htmlfilterContent = document.getElementById(checkOptions[`${evt.name}`].selectID);
    let idList = htmlfilterContent.id;
    let htmlSelect = document.createElement('div');
    htmlSelect.className = "select";
    htmlSelect.id = checkOptions[`${evt.name}`].name;
    selectList[idList][htmlSelect.id] = htmlSelect.id;
    htmlSelect.setAttribute('onclick', "showFilter(this.id)");
    let LabelTitleOptions = document.createElement('label');
    LabelTitleOptions.id = "selectText" + checkOptions[`${evt.name}`].name;
    selectList[idList][LabelTitleOptions.id] = LabelTitleOptions.id;
    LabelTitleOptions.textContent = checkOptions[`${evt.name}`].title;
    let imgArrow = document.createElement('img');
    imgArrow.className = "arrowDown";
    imgArrow.id = "arrowDown" + checkOptions[`${evt.name}`].name;
    selectList[idList][imgArrow.id] = imgArrow.id;
    imgArrow.src = "img/notification/flecha.png";
    htmlSelect.appendChild(LabelTitleOptions);
    htmlSelect.appendChild(imgArrow);
    htmlfilterContent.appendChild(htmlSelect);
    htmlOptionListContainer = document.createElement('div');
    htmlOptionListContainer.className = "optionList";
    htmlOptionListContainer.id = "optionFilter" + checkOptions[`${evt.name}`].name;
    for (let index = 0; index < dataToLoad[`${evt.name}`].length; index++) {
        let optionElement = document.createElement('div');
        optionElement.classList = "option";
        optionElement.textContent = dataToLoad[`${evt.name}`][index];
        optionElement.id = "option" + checkOptions[`${evt.name}`].name + `${index}`;
        //optionElement.setAttribute("onclick", "selectedOption(this)");
        optionElement.setAttribute("onclick", `${functionName}(this)`);
        htmlOptionListContainer.appendChild(optionElement);
    }
    htmlfilterContent.appendChild(htmlOptionListContainer);
    htmlfilterContent.style.display = "inline-block";
}

function showFilter(IDHtmlObjet) {
    checkOptions[`check${IDHtmlObjet}`].activeFilter = !checkOptions[`check${IDHtmlObjet}`].activeFilter;
    hiddenOtherSelect(IDHtmlObjet);
    let htmlOptionList = document.getElementById(`optionFilter${IDHtmlObjet}`);
    htmlOptionList.classList.toggle('show');
    let htmlArrow = document.getElementById(`arrowDown${IDHtmlObjet}`);
    htmlArrow.classList.toggle('arrowUp');

}

function hiddenOtherSelect(elementShow) {
    let activeFilters = false;
    let htmlHiddenPanel = document.getElementById("hiddenPanel");
    for (let i = 0; i < Object.getOwnPropertyNames(checkOptions).length; i++) {
        let optioName = Object.getOwnPropertyNames(checkOptions)[i];
        if (checkOptions[optioName].disable && checkOptions[optioName].name != elementShow) {
            document.getElementById("optionFilter" + checkOptions[optioName].name).classList.remove("show");
            document.getElementById("arrowDown" + checkOptions[optioName].name).classList.remove("arrowUp");
            if (checkOptions[optioName].activeFilter)
                checkOptions[optioName].activeFilter = !checkOptions[optioName].activeFilter;
        }
        if (checkOptions[optioName].activeFilter) {
            activeFilters = true;
        }
    }
    if (elementShow == 'selected' || elementShow == '') {
        activeFilters = false;
    }


    if (activeFilters) {
        htmlHiddenPanel.classList.add('showPanel');
    } else {
        htmlHiddenPanel.classList.remove('showPanel');
    }
}

function selectedOption(htmlObject) {
    let filterName = htmlObject.id.slice(6, htmlObject.id.length - 1);
    let htmlSelectText = document.getElementById(`selectText${filterName}`);
    htmlSelectText.textContent = htmlObject.textContent;
    hiddenOtherSelect('selected');

}

function removeOption(evt) {
    let htmlSelectContent = document.getElementById(checkOptions[`${evt.name}`].selectID);
    let selectRemove = document.getElementById([checkOptions[`${evt.name}`].name]);
    htmlSelectContent.removeChild(selectRemove);
    let optionRemove = document.getElementById("optionFilter" + checkOptions[`${evt.name}`].name);
    htmlSelectContent.removeChild(optionRemove);
    htmlSelectContent.style.display = "none";
    selectList[checkOptions[`${evt.name}`].selectID] = new Object();

}

function hiddenPanel() {
    hiddenOtherSelect('');
}

//#endregion
