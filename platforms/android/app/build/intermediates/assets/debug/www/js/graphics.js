

let categorias = [];// [SEMANA 23, SEMANA 24, SEMANA 25, ..., SEMANA N]
let rangeWeeks;
let seriesGraphics = [];
const listColors = ["#FF3333", "#666699", "#3366CC", "#66CC33", "#CC3366", "#6633CC", "#339999", "#993399", "#33CC66", "#0099CC", "#33CC99", "#996699", "#66FF33", "#66CC66", "#33FF66", "#FF6633", "#0099FF", "#3399CC", "#00CCCC", "#6666CC", "#FF3366", "#99CC33", "#669999", "#FF9900"];
let graphicColors = [];
let rangeAge = [
  { name: '18-24', value: 32 },
  { name: '25-34', value: 27 },
  { name: '35-44', value: 11 },
  { name: '45-54', value: 8 },
  { name: '55-64', value: 6 },
  { name: '65+', value: 5 },
  { name: 'Others', value: 11 }
];

//let jsonData = connectionServer();

function graphicsManagement() {
  fillGraphicColors();
  document.getElementById('graphicsContainer').style.display = '';
  if (filtersDashboard.city.isActive && !filtersDashboard.players.isActive &&
    !filtersDashboard.gender.isActive && !filtersDashboard.age.isActive &&
    !filtersDashboard.screens.isActive) {
    graficaCiudad(false, '');
  } else if (!filtersDashboard.city.isActive && filtersDashboard.players.isActive &&
    !filtersDashboard.gender.isActive && !filtersDashboard.age.isActive &&
    !filtersDashboard.screens.isActive) {
    graficaPlayersScreens('players');
  } else if (!filtersDashboard.city.isActive && !filtersDashboard.players.isActive &&
    filtersDashboard.gender.isActive && !filtersDashboard.age.isActive &&
    !filtersDashboard.screens.isActive) {
    document.getElementById('graphicsContainer').style.display = 'none';
    document.getElementById('gender2').style.height = '40vh';
    document.getElementById('gender2').style.marginTop = '10vh';
    graficaGenero2();
  } else if (!filtersDashboard.city.isActive && !filtersDashboard.players.isActive &&
    !filtersDashboard.gender.isActive && filtersDashboard.age.isActive &&
    !filtersDashboard.screens.isActive) {
    graficaEdad2();
  } else if (!filtersDashboard.city.isActive && !filtersDashboard.players.isActive &&
    !filtersDashboard.gender.isActive && !filtersDashboard.age.isActive &&
    filtersDashboard.screens.isActive) {
    graficaPlayersScreens('screens');
  } else if (filtersDashboard.city.isActive && filtersDashboard.players.isActive &&
    !filtersDashboard.gender.isActive && !filtersDashboard.age.isActive &&
    !filtersDashboard.screens.isActive) {
    graficaCiudad(true, 'players');
  } else if (filtersDashboard.city.isActive && !filtersDashboard.players.isActive &&
    !filtersDashboard.gender.isActive && !filtersDashboard.age.isActive &&
    filtersDashboard.screens.isActive) {
    graficaCiudad(true, 'screens');
  } else if (!filtersDashboard.city.isActive && !filtersDashboard.players.isActive &&
    filtersDashboard.gender.isActive && filtersDashboard.age.isActive &&
    !filtersDashboard.screens.isActive) {
    graficaGenero();
    document.getElementById('gender2').style.height = '40vh';
    document.getElementById('gender2').style.marginTop = '0vh';
    graficaGenero2();
  } else {
    clearElement(document.getElementById('graphicsContainer'));
    clearElement(document.getElementById('gender2'));
    document.getElementById('gender2').style.height = '0vh';
  }
}

function loadSeriesGrphics(nameFilterSelected) {
  categorias = [];
  seriesGraphics = [];
  const firstCategori = filtersDashboard.firsDate;
  const endCategori = firstCategori + rangeWeeks;
  let seriesData = [];
  let countFilters = 1;
  let elementsForGraphics = filtersDashboard[nameFilterSelected].name;
  const isGender = (nameFilterSelected == 'gender');
  if (isGender) {
    countFilters = 2;
    elementsForGraphics = ['MALE', 'FAMALE'];
  }
  if (filtersDashboard[nameFilterSelected].name == 'TOP 5' || filtersDashboard[nameFilterSelected].name == 'STAR PLAYERS' ||
    nameFilterSelected == 'screens') {
    let namesFiltersOfElements;
    namesFiltersOfElements = nameFilterSelected == 'city' ? 'cities' : 'players';
    if (nameFilterSelected == 'screens') {
      namesFiltersOfElements = 'screensName';
    }
    countFilters = filtersTools[nameFilterSelected].elements[0][namesFiltersOfElements].length;
    elementsForGraphics = filtersTools[nameFilterSelected].elements[0][namesFiltersOfElements];
  }
  if (isGender) {
    for (let i = 0; i < rangeAge.length; i++) {
      categorias.push(`${rangeAge[i].name}`);
      for (let j = 0; j < countFilters; j++) {
        seriesData[j] = (seriesData[j] == undefined) ? new Array() : seriesData[j];
        seriesData[j].push(parseInt(Math.random() * 10000));
      }
    }
  } else {
    const connectorWeeks = endCategori-firstCategori > 5? ' ': '<br>';
    for (let i = firstCategori; i <= endCategori; i++) {
      categorias.push(`${rangeNames[i].name}${connectorWeeks}${rangeNames[i].number}`);
      for (let j = 0; j < countFilters; j++) {
        seriesData[j] = (seriesData[j] == undefined) ? new Array() : seriesData[j];
        seriesData[j].push(parseInt(Math.random() * 10000));
      }
    }
  }
  for (let i = 0; i < seriesData.length; i++) {
    seriesGraphics.push(
      {
        name: countFilters == 1 ? elementsForGraphics : elementsForGraphics[i],
        data: seriesData[i],
        visible: isGender ? true : ((i == 0) ? true : false)
      }
    );
  }
}

function fillGraphicColors() {
  for (let i = 0; i < 5; i++)
    graphicColors[i] = listColors[parseInt(Math.random() * (listColors.length - 1))];
  rangeWeeks = filtersDashboard.endDate - filtersDashboard.firsDate;
}

function graficaEdad(age2) {
  if (age2 == 1) {
    let graficas = ["age21", "age22", "age23", "age24"];
    let divAge = $("#divEdades2");
  }
  else {
    let graficas = ["age1", "age2", "age3", "age4"];
    let divAge = $("#divEdades");
  }
  let g1;
  let datosGenero = [];
  let rangeAge = ["5 - 12", "13 - 17", "18 - 22", "OTHERS"]
  for (let i = 0; i < 4; i++) {
    if (ancho >= 768) {
      divAge.append('<div id="' + graficas[i] + '" style="margin-top: 5%;"></div>');
    } else {
      divAge.append('<div id="' + graficas[i] + '"></div>');
    }

    g1 = $("#" + graficas[i]);
    g1.addClass("graficasEdad");

    datosGenero = [
      {
        name: rangeAge[i],
        borderColor: Highcharts.getOptions().colors[0],
        data: [{
          color: paletaColores[parseInt(Math.random() * (paletaColores.length - 1))],
          radius: '115%',
          innerRadius: '88%',
          y: parseInt(Math.random() * 100)
        }]
      }
    ];
    Highcharts.chart(graficas[i], {
      chart: {
        type: 'solidgauge',
        marginTop: 5,
        backgroundColor: 'transparent'
      },
      credits: false,
      title: {
        text: rangeAge[i],
        floating: true,
        y: (ancho >= 768) ? 137 : 114,//145,// 114        
        widthAdjust: 0,
        style: {
          fontFamily: "Raleway",
          fontSize: (ancho >= 768) ? '16px' : '12px',// tamano de rango de edades
          color: "#b3b3b3"
        }
      },
      tooltip: {
        borderWidth: 0,
        backgroundColor: 'none',
        shadow: false,
        style: {
          fontSize: '16px'
        },
        pointFormat: '{series.name}<br><span style="font-size:2em; color: {point.color}; font-weight: bold">{point.y}%</span>',
        positioner: function (labelWidth) {
          return {
            x: g1.css("width") * 0.4,
            y: 180
          };
        },
      },
      pane: {
        startAngle: 0,
        endAngle: 360,
        background: [{ // Track for Move
          outerRadius: '112%',
          innerRadius: '88%',
          backgroundColor: Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0.0).get(),
          borderWidth: 0
        }]
      },
      yAxis: {
        min: 0,
        max: 100,
        lineWidth: 0,
        tickPositions: []
      },

      plotOptions: {
        solidgauge: {
          dataLabels: {
            enabled: true,
            y: -12,
            borderWidth: 0,
            format: "{y}%",
            style: { "color": "white" }
          },
          linecap: 'round',
          stickyTracking: false,
          rounded: true
        }
      },
      series: datosGenero
    });
  }

}// fin de la funcion producto Edad

function graficaEdad2() {
  Highcharts.chart('graphicsContainer', {
    chart: {
      type: 'pie',
      options3d: {
        enabled: true,
        alpha: 45,
        beta: 0
      }, backgroundColor: "transparent"
    },
    credits: false,
    title: {
      text: '',
      style: { "color": "white" }
    },
    tooltip: {
      pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
    },
    colors: graphicColors,
    plotOptions: {
      pie: {
        allowPointSelect: true,
        cursor: 'pointer',
        depth: 35,
        dataLabels: {
          connectorPadding: 0,
          distance: 5,
          enabled: true,
          format: '{point.name}',
          style: {
            color: "white",
            fontSize: "4vw",
            shadow: "white",
            fontFamily: "Raleway"
          }

        }
      }
    },
    series: [{
      type: 'pie',
      name: 'CONSUMPTION',
      data: [
        {
          name: rangeAge[0].name,
          y: rangeAge[0].value,
          sliced: true,
          selected: true
        },
        [rangeAge[1].name, rangeAge[1].value],
        [rangeAge[2].name, rangeAge[2].value],
        [rangeAge[3].name, rangeAge[3].value],
        [rangeAge[4].name, rangeAge[4].value],
        [rangeAge[5].name, rangeAge[5].value],
        [rangeAge[6].name, rangeAge[6].value]]
    }]
  });
}


function graficaGenero2() {
  let datosH = Math.random() * 1000;
  let datosM = Math.random() * 1000;
  let datosT = parseInt(datosM + datosH);


  Highcharts.chart('gender2', {
    chart: {
      plotBackgroundColor: null,
      plotBorderWidth: 0,
      plotShadow: false,
      backgroundColor: "transparent"
    },
    credits: false,
    title: {
      text: 'SALES: ' + datosT + ' MILLION',
      floating: true,
      widthAdjust: 100,
      y: 5,
      style: { color: "#b3b3b3", fontFamily: "Quicksand" }
    },
    tooltip: {
      pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
    },
    plotOptions: {
      pie: {
        dataLabels: {
          enabled: true,
          distance: 10,
          style: {
            fontFamily: "Quicksand",
            fontWeight: 'bold',
            color: '#b3b3b3'
          }
        },
        startAngle: -90,
        endAngle: 270,
        center: ['50%', '65%']
      }
    },
    colors: graphicColors,
    series: [{
      type: 'pie',
      name: 'SALES',
      innerSize: '50%',
      data: [
        ['BOY´S', datosH],
        ['GIRL´S', datosM],
        {
          name: 'Proprietary or Undetectable',
          y: 0.2,
          dataLabels: {
            enabled: false
          }
        }
      ]
    }]
  });

}


function graficaGenero() {
  loadSeriesGrphics('gender');
  Highcharts.chart('graphicsContainer', {
    chart: {
      type: 'column',
      options3d: {
        enabled: true,
        alpha: 10,
        beta: 25,
        depth: 70
      },
      backgroundColor: 'transparent'
    },
    credits: false,
    legend: {
      itemHiddenStyle: { "color": "#a6a6a6", fontFamily: "Raleway" },
      itemHoverStyle: { "color": "white", fontFamily: "Raleway" },
      itemStyle: {
        "color": "white",
        "cursor": "pointer",
        "fontSize": "14px",
        "fontWeight": "bold",
        "shadow": "red",
        fontFamily: "Raleway"
      }
    },
    title: {
      text: 'SOLD PRODUCTS',
      style: { color: "#ffffff", fontFamily: "Raleway" }
    },
    subtitle: {
      text: null
    },
    plotOptions: {
      column: {
        depth: 20
      }
    },
    xAxis: {
      categories: categorias,
      labels: {
        style: {
          color: 'rgba(255,255,255,0.7)',
          fontFamily: 'Montserrat',
          fontSize:'3vw',
          fontWeight: '400'
        }
      }
    },
    yAxis: {
      title: {
        text: null
      },
      labels: {
        style: {
          color: 'rgba(255,255,255,0.7)',
          fontFamily: 'Montserrat',
          fontSize:'3vw',
          fontWeight: '400'
        }
      }
    },
    colors: graphicColors,
    series: seriesGraphics
  });
}

function graficaPlayersScreens(filterSelected) {
  loadSeriesGrphics(filterSelected);

  Highcharts.chart('graphicsContainer', {
    chart: {
      type: 'spline',
      backgroundColor: 'transparent'
    },
    credits: false,
    title: {
      text: filtersDashboard[filterSelected].name,
      style: { color: "white", fontFamily: "Raleway" }
    },
    subtitle: {
      text: ''
    },
    xAxis: {
      className: "colorGraphX",
      categories: categorias,
      labels: {
        style: {
          color: 'rgba(255,255,255,0.7)',
          fontFamily: 'Montserrat',
          fontSize:'3vw',
          fontWeight: '400'
        }
      }      
    },
    yAxis: {
      className: "colorGraphX",
      title: {
        text: 'SOLD PRODUCTS',
        style: { color: "black", fontFamily: "Raleway" }
      },
      labels: {
        formatter: function () {
          return this.value;
        }
      },
      labels: {
        style: {
          color: 'rgba(255,255,255,0.7)',
          fontFamily: 'Montserrat',
          fontSize:'3vw',
          fontWeight: '400'
        }
      }
    },
    tooltip: {
      crosshairs: true,
      shared: true
    },
    plotOptions: {
      spline: {
        marker: {
          radius: 4,
          lineColor: '#666666',
          lineWidth: 1
        }
      }
    },
    legend: {
      itemHiddenStyle: { "color": "#a6a6a6", fontFamily: "Raleway" },
      itemHoverStyle: { "color": "white", fontFamily: "Raleway" },
      itemStyle: {
        "color": "white",
        "cursor": "pointer",
        "fontSize": "14px",
        "fontWeight": "bold",
        "shadow": "red",
        fontFamily: "Raleway"
      }
    },
    colors: graphicColors,
    series: seriesGraphics
  });

}// fin de la gráfica players


function graficaCiudad(auxSubtitle, subFilter) {

  if (subFilter == 'screens') {
    loadSeriesGrphics('screens');
  } else {
    loadSeriesGrphics('city');
  }

  Highcharts.chart('graphicsContainer', {
    chart: {
      type: 'area',
      backgroundColor: 'transparent'
    },
    legend: {
      itemHiddenStyle: { "color": "#a6a6a6", fontFamily: "Raleway" },
      itemHoverStyle: { "color": "white", fontFamily: "Raleway" },
      itemStyle: {
        "color": "white",
        "cursor": "pointer",
        "fontSize": "14px",
        "fontWeight": "bold",
        "shadow": "red",
        fontFamily: "Raleway"
      }
    },
    title: {
      text: filtersDashboard.city.name,
      style: { color: "white", fontFamily: "Raleway" }
    },
    credits: false,
    subtitle: {
      text: (auxSubtitle ? filtersDashboard[subFilter].name : ''),
      style: { color: "white", fontFamily: "Raleway" }
    },
    colors: graphicColors,
    xAxis: {
      className: "colorGraphX",
      categories: categorias,
      labels: {
        style: {
          color: 'rgba(255,255,255,0.7)',
          fontFamily: 'Montserrat',
          fontSize:'3vw',
          fontWeight: '400'
        }
      }
    },
    yAxis: {
      className: "colorGraphY",
      title: {
        text: 'SOLD PRODUCTS',
        style: { color: "#b3b3b3", fontFamily: "Raleway", fontSize: "15px" }
      },
      labels: {
        formatter: function () {
          return this.value / 1000 + 'k';
        }
      },
      min: 0,
      labels: {
        style: {
          color: 'rgba(255,255,255,0.7)',
          fontFamily: 'Montserrat',
          fontSize:'3vw',
          fontWeight: '400'
        }
      }
    },
    tooltip: {
      pointFormat: 'SOLD PRODUCTS; <b> {point.y:,.0f}, </b> {series.name}'
    },
    plotOptions: {
      area: {
        fillOpacity: 0.6
      }
    },
    series: seriesGraphics
  });
}// fin de la gráfica ciudad

