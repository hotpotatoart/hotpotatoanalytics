
let initialIndexSelected;
let maxRange = 12;
let minRange = maxRange - 12;
let indexMonthStart;
let indexMonthEnd;
let monthStart;
let monthEnd;
let startDraw;
let endDraw;
let rectMonths = new Array();
let rangeNames = new Array();
let indexTouchStart;
let indexTouchEnd;

function prepareTimeLine() {
  let divTimeLine = document.getElementById('timeLine');
  createRangeNames();
  loadIndexRange();
  resetTimeLine(divTimeLine);
  createEventsTimeLine(divTimeLine);
  getMonthRects();
}

function touchstartTimeLine(evt) {
  let idElement = `${evt.targetTouches[evt.targetTouches.length - 1].target.id}`;  
  if (idElement == 'timeLine') {
    indexTouchStart = -1;
  } else {    
    let textTouch = document.getElementById(idElement).textContent;
    let elementTouch = { name: textTouch.substring(0, 4), number: textTouch.substring(4, 7) };
    //alert(`${JSON.stringify(elementTouch)}`);
    indexTouchStart = rangeNames.findIndex((element) => {
      return element.name == elementTouch.name && element.number == elementTouch.number;
    });
  }
  if (indexTouchStart != indexMonthStart && indexTouchStart != indexMonthEnd) {
    indexTouchStart = evt.touches[0].clientX;
    monthStart = -1;
    return;
  }
  if (indexTouchStart == indexMonthStart) {
    initialIndexSelected = true;
  } else {
    initialIndexSelected = false;
  }
  monthStart = document.getElementById(`${idElement}`).getBoundingClientRect();
}

function touchEndTimeLine(evt) {
  if (monthStart == -1) {
    moveTimeLine(indexTouchEnd);
    return;
  }
  spacesUpdated = -1;
  sortIndexes();
  filtersDashboard.firsDate = indexMonthStart;// variable de dashboard.js
  filtersDashboard.endDate = indexMonthEnd;// variable de dashboard.js
  graphicsManagement();// funcion que actualiza la gráfica
  let aux1 = `${rangeNames[indexMonthStart].name}/${rangeNames[indexMonthStart].number}`;
  let aux2 = `${rangeNames[indexMonthEnd].name}/${rangeNames[indexMonthEnd].number}`;
  if (indexMonthStart >= 0 && indexMonthEnd >= 0) {
    window.localStorage.setItem("rangeTimeLine", JSON.stringify([indexMonthStart, indexMonthEnd]));
  }
}

function touchMoveTimeLine(evt) {
  if (monthStart == -1) {
    indexTouchEnd = evt.touches[0].clientX;
    return;
  }
  searchMonthEnd(evt.touches[0].clientX);
  updateMonthActive();
}

function searchMonthEnd(monthTouched) {
  let indexMonth = foundIndexTouch(monthTouched);
  if (indexMonth == -1) {
    if (monthTouched < rectMonths[0].left) {
      indexMonth = 0;
    } else {
      indexMonth = 11;
    }
  }
  if (initialIndexSelected) {
    indexMonthStart = indexMonth + minRange;
    startDraw = indexMonth;
  } else {
    indexMonthEnd = indexMonth + minRange;
    endDraw = indexMonth;
  }
  monthEnd = rectMonths[indexMonth];
}

function updateMonthActive() {
  let monthsList = document.getElementsByClassName('month');
  for (let i = 0; i < monthsList.length; i++) {
    if (i >= startDraw && i <= endDraw || i <= startDraw && i >= endDraw) {
      monthsList[i].classList.add('monthActive');
    } else {
      monthsList[i].classList.remove('monthActive');
    }
  }
}

function updateTimeLine(add) {
  minRange += add;
  maxRange += add;
  startDraw -= add;
  endDraw -= add;
  if (!(minRange > -1 && maxRange <= rangeNames.length)) {
    minRange -= add;
    maxRange -= add;
    startDraw += add;
    endDraw += add;
    return;
  }
  let divTimeLine = document.getElementById('timeLine');
  resetTimeLine(divTimeLine);
  updateMonthActive();
}

function moveTimeLine() {
  if (indexTouchStart < rectMonths[0].left || indexTouchStart > rectMonths[rectMonths.length - 1].right) {
    return;
  }
  const monthWidth = rectMonths[0].width;
  const sideTimeline = (indexTouchStart > indexTouchEnd) ? 1 : -1;
  const touchDistance = Math.abs(indexTouchStart - indexTouchEnd);
  const spacesMoved = Math.trunc(touchDistance / monthWidth);
  for (var i = 0; i <= spacesMoved; i++) {
    setTimeout(() => {
      updateTimeLine(sideTimeline);
    }, i * 100)
  }
}

function sortIndexes() {
  if (indexMonthStart > indexMonthEnd) {
    let max = indexMonthStart;
    indexMonthStart = indexMonthEnd;
    indexMonthEnd = max;
  }
  if (startDraw > endDraw) {
    let maxDraw = startDraw;
    startDraw = endDraw;
    endDraw = maxDraw;
  }
}

function foundIndexTouch(touchedElement) {
  for (let i = 0; i < rectMonths.length; i++) {
    if (touchedElement > rectMonths[i].left && touchedElement <= rectMonths[i].right) {
      return i;
      break;
    }
  }
  return -1;
}

function showRangeTimeLine(){
  const vars = {
    'indexMonthStart': indexMonthStart,
    'indexMonthEnd': indexMonthEnd,
    'minRange': minRange,
    'maxRange': maxRange,
    'startDraw':startDraw,
    'endDraw':endDraw
  };
  alert(`${JSON.stringify(vars)}`);
}

function loadIndexRange() {
  const countRangeName = rangeNames.length;
  const itemsTimeLine = 12;

  let rangeTimeLine = window.localStorage.getItem("rangeTimeLine");
  if (rangeTimeLine != null) {
    rangeTimeLine = JSON.parse(rangeTimeLine);
    indexMonthStart = rangeTimeLine[0];
    indexMonthEnd = rangeTimeLine[1];
  } else {
    indexMonthStart = 0;
    indexMonthEnd = 4;
  }
  startDraw = 0;
  endDraw = indexMonthEnd - indexMonthStart;
  minRange = indexMonthStart; 
  maxRange = minRange + itemsTimeLine;
  if (maxRange >= countRangeName) {
    maxRange = countRangeName;
    minRange = maxRange - itemsTimeLine;
    startDraw = Math.abs(indexMonthStart - (countRangeName-itemsTimeLine) );
    endDraw = startDraw + (indexMonthEnd - indexMonthStart);
  } 
  
}

function resetTimeLine(timeLine) {
  const existContainer = document.getElementById('containerMonths');
  if (existContainer != null) {
    timeLine.removeChild(existContainer);
  }
  let containerMonths = document.createElement('table');  
  let colTable = document.createElement('tr');
  colTable.setAttribute('align', 'center');
  let rowTable;
  containerMonths.setAttribute('id', 'containerMonths');
  for (let i = minRange; i < maxRange; i++) {
    rowTable = document.createElement('td');
    rowTable.setAttribute('id', `month${i}`);
    rowTable.setAttribute('class', 'month');
    rowTable.innerHTML = `${rangeNames[i].name}<br>${rangeNames[i].number}`;
    colTable.appendChild(rowTable);
  }
  containerMonths.appendChild(colTable);
  timeLine.appendChild(containerMonths);
}

function getMonthRects() {
  let monthsList = document.getElementsByClassName('month');
  for (let i = 0; i < monthsList.length; i++) {
    rectMonths.push(monthsList[i].getBoundingClientRect());
  }
}

function createEventsTimeLine(divTimeLine) {
  divTimeLine.ontouchstart = touchstartTimeLine;
  divTimeLine.ontouchmove = touchMoveTimeLine;
  divTimeLine.ontouchend = touchEndTimeLine;
}

function createRangeNames() {
  rangeNames.push({ name: 'W-22', number: 'JUN' });//0
  rangeNames.push({ name: 'W-23', number: 'JUN' });//1
  rangeNames.push({ name: 'W-24', number: 'JUN' });//2
  rangeNames.push({ name: 'W-25', number: 'JUN' });//3
  rangeNames.push({ name: 'W-26', number: 'JUN' });//4
  rangeNames.push({ name: 'W-27', number: 'JUL' });//5
  rangeNames.push({ name: 'W-28', number: 'JUL' });//6
  rangeNames.push({ name: 'W-29', number: 'JUL' });//7
  rangeNames.push({ name: 'W-30', number: 'JUL' });//8
  rangeNames.push({ name: 'W-31', number: 'AUG' });//9
  rangeNames.push({ name: 'W-32', number: 'AUG' });//10
  rangeNames.push({ name: 'W-33', number: 'AUG' });//11
  rangeNames.push({ name: 'W-34', number: 'AUG' });//12
  rangeNames.push({ name: 'W-35', number: 'AUG' });//13
}